-- 用户表
DROP TABLE IF EXISTS t_user;
CREATE TABLE t_user (
       id                   VARCHAR(36)              NOT NULL PRIMARY KEY    ,             -- ID,主键
       email                VARCHAR(100)             NOT NULL                ,             -- 邮箱/用户名
       nickname             VARCHAR(300)                                     ,             -- 昵称
       password             VARCHAR(100)             NOT NULL                ,             -- 用户密码
       gender               SMALLINT                 DEFAULT 0               ,             -- 性别（0：保密，1：男，2：女）
       age                  INTEGER                  DEFAULT 100             ,             -- 年龄
       address              VARCHAR(255)                                     ,             -- 地址
       intro                VARCHAR(255)                                     ,             -- 用户介绍
       money                INTEGER                  DEFAULT 0               ,             -- 金币
       level                INTEGER                  DEFAULT 0               ,             -- 等级
       icon_path            VARCHAR(255)                                     ,             -- 用户头像保存路径
       is_admin             BOOLEAN                  DEFAULT FALSE           ,             -- 是否为超级管理员
       create_time          BIGINT                   NOT NULL                ,             -- 用户创建（注册）时间
       is_del               BOOLEAN                  NOT NULL DEFAULT FALSE                -- 是否已删除
);

CREATE INDEX t_user_username_index ON t_user(email);
CREATE INDEX t_user_password_index ON t_user(password);
CREATE INDEX t_user_create_time_index ON t_user(create_time);

INSERT INTO t_user(id,email,nickname,password,gender,age,is_admin,create_time) VALUES('555062ca5112b41c6ccf59a4','admin@daervin.com','我是超人',
'e10adc3949ba59abbe56e057f20f883e',0,100,TRUE,1431331530683);


-- 问卷表
DROP TABLE IF EXISTS t_survey;
CREATE TABLE t_survey (
       id                   VARCHAR(36)              PRIMARY KEY             ,             -- ID,主键
       templet_id           VARCHAR(36)                                      ,             -- 模板ID,外键
       title                VARCHAR(200)             NOT NULL                ,             -- 问卷标题
       sdesc                TEXT                     NOT NULL                ,             -- 问卷描述
       author               VARCHAR(100)             NOT NULL                ,             -- 问卷所属用户的用户名
       author_id            VARCHAR(36)              NOT NULL                ,             -- 问卷所属用户的ID
       create_time          BIGINT                   NOT NULL                ,             -- 用户创建时间 
       expire_time          BIGINT                   NOT NULL                ,             -- 问卷结束时间 
       password             VARCHAR(100)                                     ,             -- 非公开问卷访问密码
       is_open              BOOLEAN                  NOT NULL DEFAULT TRUE   ,             -- 是否公开
       hits                 INTEGER                  NOT NULL DEFAULT 0      ,             -- 浏览数
       usehits              INTEGER                  NOT NULL DEFAULT 0      ,             -- 答卷数
       que_num              INTEGER                  NOT NULL DEFAULT 0      ,             -- 题目数
       is_del               BOOLEAN                  NOT NULL DEFAULT FALSE                -- 是否已删除
);

CREATE INDEX t_survey_author_index ON t_survey(author);
CREATE INDEX t_survey_author_id_index ON t_survey(author_id);
CREATE INDEX t_survey_create_time_index ON t_survey(create_time);
CREATE INDEX t_survey_expire_time_index ON t_survey(expire_time);


-- 答卷表
DROP TABLE IF EXISTS t_answer;
CREATE TABLE t_answer (
       id                   VARCHAR(36)              PRIMARY KEY             ,             -- ID,主键
       survey_id            VARCHAR(36)              NOT NULL                ,             -- 问卷ID,外键
       user_id              VARCHAR(36)              NOT NULL                ,             -- 所属用户的ID,外键
       result               TEXT                     NOT NULL                ,             -- 单个用户的答案（题号(qnum):选项答案(oprelt)=答案1,答案2;文本答案(text)=输入的文本|#|题号:选项答案=答案1,答案2;文本答案=输入的文本）
       post_time            BIGINT                   NOT NULL                ,             -- 答卷提交时间 
       is_del               BOOLEAN                  NOT NULL DEFAULT FALSE                -- 是否已删除
);

CREATE INDEX t_answer_survey_id_index ON t_answer(survey_id);
CREATE INDEX t_answer_user_id_index ON t_answer(user_id);


-- 题目表
DROP TABLE IF EXISTS t_question;
CREATE TABLE t_question (
       id                   VARCHAR(36)              PRIMARY KEY             ,             -- ID,主键
       survey_id            VARCHAR(36)              NOT NULL                ,             -- 问卷ID,外键
       type                 SMALLINT                 NOT NULL                ,             -- 类型（0：问答，1：单选，2：多选，3：单选+自定义输入，4：多选+自定义输入）
       head                 VARCHAR(255)             NOT NULL                ,             -- 题干
       create_time          BIGINT                   NOT NULL                ,             -- 创建时间 
       use_num              INTEGER                  NOT NULL DEFAULT 0      ,             -- 被选次数
       is_del               BOOLEAN                  NOT NULL DEFAULT FALSE                -- 是否已删除
);

CREATE INDEX t_question_survey_id_index ON t_question(survey_id);


-- 选项表
DROP TABLE IF EXISTS t_options;
CREATE TABLE t_options (
       id                   VARCHAR(36)              PRIMARY KEY             ,             -- ID,主键
       survey_id            VARCHAR(36)              NOT NULL                ,             -- 问卷ID,外键
       question_id          VARCHAR(36)              NOT NULL                ,             -- 题目ID,外键
       content              VARCHAR(255)             NOT NULL                ,             -- 选项内容
       img                  VARCHAR(255)                                     ,             -- 选项展示图片
       order_num            SMALLINT                 NOT NULL                ,             -- 选项序号
       use_num              INTEGER                  NOT NULL DEFAULT 0      ,             -- 被选次数
       is_del               BOOLEAN                  NOT NULL DEFAULT FALSE                -- 是否已删除
);

CREATE INDEX t_options_survey_id_index ON t_options(survey_id);
CREATE INDEX t_options_question_id_index ON t_options(question_id);


-- 文本答案表
DROP TABLE IF EXISTS t_text;
CREATE TABLE t_text (
       id                   VARCHAR(36)              PRIMARY KEY             ,             -- ID,主键
       survey_id            VARCHAR(36)              NOT NULL                ,             -- 问卷ID,外键
       question_id          VARCHAR(36)              NOT NULL                ,             -- 题目ID,外键
       content              VARCHAR(255)             NOT NULL                ,             -- 
       is_del               BOOLEAN                  NOT NULL DEFAULT FALSE                -- 是否已删除
);

CREATE INDEX t_text_survey_id_index ON t_text(survey_id);
CREATE INDEX t_text_question_id_index ON t_text(question_id);


-- 提交事务
COMMIT;