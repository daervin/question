/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.controller.manager;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.dub.common.shiro.ShiroUser;
import cn.dub.common.utils.CurrentUserUtils;
import cn.dub.entity.manager.survey.Survey;
import cn.dub.entity.show.User;
import cn.dub.service.manager.admin.IManagerUserService;
import cn.dub.service.manager.survey.ISurveyService;

/**
 * 超级管理界面控制器。
 * 
 * @author daervin (2015年5月18日 下午3:27:54)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
@Controller
@RequestMapping("/manager/admin")
public class AdminController {

    @Inject
    private IManagerUserService userService;

    @Inject
    private ISurveyService surveyService;

    /**
     * 管理首页。
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    @RequestMapping("/index")
    public String index(ModelMap model) {
        ShiroUser user = CurrentUserUtils.getCurrentUser(); // 获取当前登录用户信息
        model.addAttribute("user", user);

        return "manager/admin/index";
    }

    /**
     * 用户列表。
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    @RequestMapping("/userlist")
    public String userList(ModelMap model) {
        List<User> pageInfo = this.userService.findsUser();
        model.addAttribute("pageInfo", pageInfo);

        return "manager/admin/userlist";
    }

    /**
     * 根据ID删除一个用户。
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    @ResponseBody
    @RequestMapping("/deleteuser/{id}")
    public boolean delete(@PathVariable("id") String id, ModelMap model) {
        return this.userService.deleteById(id);
    }

    /**
     * 问卷列表。
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    @RequestMapping("/surveylist")
    public String surveyList(ModelMap model) {
        List<Survey> pageInfo = this.surveyService.findsAll();
        model.addAttribute("pageInfo", pageInfo);

        return "manager/admin/surveyList";
    }

}
