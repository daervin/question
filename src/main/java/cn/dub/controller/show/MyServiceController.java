/**
* <p>Project: questionnaire</p>
* <p>Copyright: Copyright (c) 2015 7dub.cn</p>
* <p>Company: 柒色都吧7dub.cn</p>
*/

package cn.dub.controller.show;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.dub.common.shiro.ShiroUser;
import cn.dub.common.utils.CurrentUserUtils;

/**
 * 本站服务控制器。
 * 
 * @author daervin (2015年5月16日 下午1:19:10)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
@Controller
public class MyServiceController {

    @RequestMapping("/service")
    public String service(ModelMap model) {
        ShiroUser user = CurrentUserUtils.getCurrentUser(); // 获取当前登录用户信息
        model.addAttribute("user", user);
        return "service/index";
    }
}
