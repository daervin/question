/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.controller.show;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.dub.common.utils.EncryptUtils;
import cn.dub.entity.show.User;
import cn.dub.form.show.session.LoginForm;
import cn.dub.form.show.session.RegisterForm;
import cn.dub.service.show.IUserService;

/**
 * Session控制器。
 *
 * @author daervin(2015年3月12日 下午2:18:18)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
@Controller
@RequestMapping(value = "/session")
public class SessionControllor {

    @Inject
    private IUserService userService;

    /**
     * 登录页面展示。
     * 
     * @since 1.0.0
     * @version 1.0.0
     */
    @RequestMapping("/login")
    public String loginView() {
        return "session/login";
    }

    /**
     * 登录处理。
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    @ResponseBody
    @RequestMapping(value = "/login/add", method = RequestMethod.POST)
    public boolean login(LoginForm form) {

        Subject currentUser = SecurityUtils.getSubject();

        UsernamePasswordToken token = new UsernamePasswordToken(form.getEmail(), EncryptUtils.encryptMD5(form.getPassword()));
        // token.setRememberMe(true);

        boolean msg = false; // 提示信息
        try {
            currentUser.login(token);
        }
        catch (AuthenticationException e) {
            // e.printStackTrace();
        }

        // 登录成功
        if (currentUser.isAuthenticated()) {
            msg = true;
        }

        return msg;
    }

    /**
     * 登出处理。
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    @RequestMapping("/logout")
    public String logout() {
        Subject currentUser = SecurityUtils.getSubject();
        currentUser.logout();
        return "redirect:/home";
    }

    /**
     * 注册页面展示。
     * 
     * @since 1.0.0
     * @version 1.0.0
     */
    @RequestMapping("/register")
    public String registerView() {
        return "session/register";
    }

    /**
     * 注册处理。
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    @ResponseBody
    @RequestMapping(value = "/register/add", method = RequestMethod.POST)
    public boolean register(RegisterForm form) {
        return this.userService.insert(form);
    }

    /**
     * 校验邮箱。
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    @ResponseBody
    @RequestMapping("/check/email")
    public boolean checkEmail(String email) {
        if (StringUtils.isBlank(email)) {
            return false;
        }
        User user = this.userService.findByEmail(email);
        if (user != null) {
            return false;
        }
        return true;
    }
}
