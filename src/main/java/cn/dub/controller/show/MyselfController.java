/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.controller.show;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.dub.common.utils.CurrentUserUtils;
import cn.dub.entity.manager.survey.Survey;
import cn.dub.entity.show.User;
import cn.dub.form.manager.survey.FindSurveyListForm;
import cn.dub.form.show.session.EditForm;
import cn.dub.service.manager.survey.ISurveyService;
import cn.dub.service.show.IUserService;

import com.github.pagehelper.PageInfo;

@Controller
@RequestMapping("/myself")
public class MyselfController {

    @Inject
    private IUserService userService;

    @Inject
    private ISurveyService surveyService;

    /**
     * 我的空间首页控制器。
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    @RequestMapping("/index")
    public String index(FindSurveyListForm form, ModelMap model) {
        User user = this.userService.findByEmail(CurrentUserUtils.getCurrentUser().getEmail());// 获取当前登录用户信息
        model.addAttribute("user", user);

        form.setUid(user.getId());
        model.addAttribute("form", form);

        PageInfo<Survey> surveyPageInfo = this.surveyService.findsAllByParameter(form);
        model.addAttribute("surveyPageInfo", surveyPageInfo);

        return "myself/index";
    }

    /**
     * 个人资料编辑页面。
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    @RequestMapping("/editView")
    public String editView(ModelMap model) {
        User user = this.userService.findByEmail(CurrentUserUtils.getCurrentUser().getEmail());// 获取当前登录用户信息
        model.addAttribute("user", user);

        return "myself/edit";
    }

    /**
     * 个人资料编辑。
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    @ResponseBody
    @RequestMapping("/edit")
    public boolean edit(EditForm form) {
        return this.userService.editCurrentUser(form);
    }

}
