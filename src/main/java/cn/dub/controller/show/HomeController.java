/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.controller.show;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.dub.common.shiro.ShiroUser;
import cn.dub.common.utils.CurrentUserUtils;

/**
 * 主页控制器。
 *
 * @author daervin(2015年3月12日 下午2:17:31)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
@Controller
public class HomeController {

    @RequestMapping("/home")
    public String home(ModelMap model) {
        ShiroUser user = CurrentUserUtils.getCurrentUser(); // 获取当前登录用户信息
        model.addAttribute("user", user);
        return "home/index";
    }

    @RequestMapping("/error")
    public String error(ModelMap model) {
        model.addAttribute("info", "错误!");
        return "home/index";
    }
}
