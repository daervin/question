/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.form.manager.survey;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/**
 * 新增题目和选项的数据保存。
 * 
 * @author daervin (2015年5月12日 下午4:41:07)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
public class AddQuestionAndOptionsForm {

    private String surveyId; // 问卷ID,外键
    private String type; // 类型（0：问答，1：单选，2：多选，3：单选+自定义输入，4：多选+自定义输入）
    private String head; // 题干

    private String contentA; // 选项内容A
    private String imgA; // 选项展示图片A
    private String orderNumA; // 选项序号A

    private String contentB; // 选项内容B
    private String imgB; // 选项展示图片B
    private String orderNumB; // 选项序号B

    private String contentC; // 选项内容C
    private String imgC; // 选项展示图片C
    private String orderNumC; // 选项序号C

    private String contentD; // 选项内容D
    private String imgD; // 选项展示图片D
    private String orderNumD; // 选项序号D

    private String contentE; // 选项内容E
    private String imgE; // 选项展示图片E
    private String orderNumE; // 选项序号E

    private String contentF; // 选项内容F
    private String imgF; // 选项展示图片F
    private String orderNumF; // 选项序号F

    @NotBlank
    @Pattern(regexp = "^\\w{24}$", message = "ID格式错误")
    public String getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(String surveyId) {
        this.surveyId = surveyId;
    }

    @Min(1)
    @Max(4)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @NotBlank(message = "标题不能为空！")
    @Length(min = 1, max = 200)
    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    @Length(min = 1, max = 200)
    public String getContentA() {
        return contentA;
    }

    public void setContentA(String contentA) {
        this.contentA = contentA;
    }

    public String getImgA() {
        return imgA;
    }

    public void setImgA(String imgA) {
        this.imgA = imgA;
    }

    @Min(0)
    @Max(6)
    public String getOrderNumA() {
        return orderNumA;
    }

    public void setOrderNumA(String orderNumA) {
        this.orderNumA = orderNumA;
    }

    @Length(min = 1, max = 200)
    public String getContentB() {
        return contentB;
    }

    public void setContentB(String contentB) {
        this.contentB = contentB;
    }

    public String getImgB() {
        return imgB;
    }

    public void setImgB(String imgB) {
        this.imgB = imgB;
    }

    @Min(0)
    @Max(6)
    public String getOrderNumB() {
        return orderNumB;
    }

    public void setOrderNumB(String orderNumB) {
        this.orderNumB = orderNumB;
    }

    @Length(min = 1, max = 200)
    public String getContentC() {
        return contentC;
    }

    public void setContentC(String contentC) {
        this.contentC = contentC;
    }

    public String getImgC() {
        return imgC;
    }

    public void setImgC(String imgC) {
        this.imgC = imgC;
    }

    @Min(0)
    @Max(6)
    public String getOrderNumC() {
        return orderNumC;
    }

    public void setOrderNumC(String orderNumC) {
        this.orderNumC = orderNumC;
    }

    @Length(min = 1, max = 200)
    public String getContentD() {
        return contentD;
    }

    public void setContentD(String contentD) {
        this.contentD = contentD;
    }

    public String getImgD() {
        return imgD;
    }

    public void setImgD(String imgD) {
        this.imgD = imgD;
    }

    @Min(0)
    @Max(6)
    public String getOrderNumD() {
        return orderNumD;
    }

    public void setOrderNumD(String orderNumD) {
        this.orderNumD = orderNumD;
    }

    @Length(min = 1, max = 200)
    public String getContentE() {
        return contentE;
    }

    public void setContentE(String contentE) {
        this.contentE = contentE;
    }

    public String getImgE() {
        return imgE;
    }

    public void setImgE(String imgE) {
        this.imgE = imgE;
    }

    @Min(0)
    @Max(6)
    public String getOrderNumE() {
        return orderNumE;
    }

    public void setOrderNumE(String orderNumE) {
        this.orderNumE = orderNumE;
    }

    @Length(min = 1, max = 200)
    public String getContentF() {
        return contentF;
    }

    public void setContentF(String contentF) {
        this.contentF = contentF;
    }

    public String getImgF() {
        return imgF;
    }

    public void setImgF(String imgF) {
        this.imgF = imgF;
    }

    @Min(0)
    @Max(6)
    public String getOrderNumF() {
        return orderNumF;
    }

    public void setOrderNumF(String orderNumF) {
        this.orderNumF = orderNumF;
    }

}
