/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.form.manager.survey;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

/**
 * 提交答卷数据保存。
 * 
 * @author daervin (2015年5月14日 下午7:53:54)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
public class AddAnswerForm {

    private String surveyId; // 问卷ID
    /**
     * 题号(qnum):选项答案(oprelt)=答案1,答案2;文本答案(text)=输入的文本|#|题号:选项答案=答案1,答案2;文本答案=输入的文本 
     * 例如 ：001:oprelt=2;text=|#|002:oprelt=1,3,5;text=自定义文本
     */
    private String result; // 答案信息

    @NotBlank
    @Pattern(regexp = "^\\w{24}$", message = "ID格式错误")
    public String getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(String surveyId) {
        this.surveyId = surveyId;
    }

    @NotBlank(message="你没有作答！")
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

}
