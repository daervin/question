/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.form.manager.survey;

/**
 * 新增问卷数据接收类。
 * 
 * @author daervin (2015年5月11日 下午12:12:40)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
public class AddSurveyForm {
    private String title;// 问卷标题
    private String sdesc;// 问卷描述
    private String expireTime;// 问卷结束时间
    private String isOpen;// 是否公开
    private String password;// 非公开问卷访问密码

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSdesc() {
        return sdesc;
    }

    public void setSdesc(String sdesc) {
        this.sdesc = sdesc;
    }

    public String getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(String expireTime) {
        this.expireTime = expireTime;
    }

    public String getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(String isOpen) {
        this.isOpen = isOpen;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
