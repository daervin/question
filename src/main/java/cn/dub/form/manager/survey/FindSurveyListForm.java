/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.form.manager.survey;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import cn.dub.common.form.PageForm;

/**
 * 查找列表所用参数表单数据。
 * 
 * @author daervin (2015年5月14日 上午9:28:11)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
public class FindSurveyListForm extends PageForm {

    private String title; // 问卷标题关键字
    private String isOpen; // 是否公开
    private String uid; // 发布者ID

    @Length(min = 0, max = 100)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Pattern(regexp = "(|true|false)", message = "邮件格式错误")
    public String getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(String isOpen) {
        this.isOpen = isOpen;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

}
