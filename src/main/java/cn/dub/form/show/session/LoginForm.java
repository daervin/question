/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.form.show.session;

/**
 * 用户登录数据保存。
 * 
 * @author daervin (2015年4月17日 下午9:16:10)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
public class LoginForm {
    private String email; // 邮箱（用户名）
    private String password; // 密码

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
