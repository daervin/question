/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.dao.show;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.dub.common.annotation.MybatisDao;
import cn.dub.entity.show.User;

/**
 * 用户表数据访问接口。
 *
 * @author daervin(2015年3月12日 下午5:15:19)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
@MybatisDao
public interface IUserMybatisDao {

    /**
     * 新增用戶。
     *
     * @param user
     *            {@linkplain cn.dub.entity.show.User} 对象
     * @return 受影响行数
     *
     */
    public int insert(User user);

    /**
     * 根据邮箱查找用户。
     * 
     * @param email
     *            邮箱
     * @return {@linkplain cn.dub.entity.show.User} 对象
     */
    public User selectByEmail(String email);

    /**
     * 根据ID更新用户信息。
     *
     * @param user
     *            {@linkplain cn.dub.entity.show.User} 对象
     * @return 受影响行数
     *
     */
    public int updateById(@Param("id") String id, @Param("nickname") String nickname, @Param("address") String address, @Param("intro") String intro, @Param("age") int age, @Param("gender") int gender);

    /**
     * 用户列表。
     *
     * @return {@linkplain cn.dub.entity.show.User}列表
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    public List<User> selectsAll();

    /**
     * 根据ID删除用户。
     *
     * @param id
     *            用户ID
     * @return 受影响行数
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    public int deleteById(String id);
}
