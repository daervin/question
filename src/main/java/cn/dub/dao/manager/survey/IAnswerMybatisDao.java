/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.dao.manager.survey;

import cn.dub.common.annotation.MybatisDao;
import cn.dub.entity.manager.survey.Answer;

/**
 * 答卷表数据访问借口。
 * 
 * @author daervin (2015年5月15日 下午1:00:54)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
@MybatisDao
public interface IAnswerMybatisDao {

    /**
     * 新增答卷。
     *
     * @param answer
     *            {@linkplain cn.dub.entity.manager.survey.Answer}答卷实体
     * @return 受影响行数
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    public int insert(Answer answer);

    /**
     * 统计单个用户对单个问卷的总数。
     *
     * @param answer
     *            {@linkplain cn.dub.entity.manager.survey.Answer}答卷实体
     * @return 受影响行数
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    public int selectCount(Answer answer);

}
