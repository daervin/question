/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.dao.manager.survey;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.dub.common.annotation.MybatisDao;
import cn.dub.entity.manager.survey.Survey;

/**
 * 问卷表数据访问接口。
 * 
 * @author daervin (2015年5月11日 下午3:36:58)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
@MybatisDao
public interface ISurveyMybatisDao {

    /**
     * 新增空白问卷。
     *
     * @param survey
     *            {@linkplain cn.dub.entity.manager.survey.Survey}问卷实体类
     * @return 受影响行数
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    public int insert(Survey survey);

    /**
     * 根据主键查找单个问卷。
     *
     * @param id
     *            问卷ID
     * @return {@linkplain cn.dub.entity.manager.survey.Survey}问卷实体类
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    public Survey selectById(String id);

    /**
     * 根据参数查找问卷列表（参数均为空则查全部）。
     *
     * @param title
     *            问卷标题关键字
     * @param isOpen
     *            是否公开
     * @return {@linkplain cn.dub.entity.manager.survey.Survey}问卷列表
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    public List<Survey> selectsAllByParameter(@Param("title") String title, @Param("isOpen") Boolean isOpen, @Param("uid") String uid);

    /**
     * 问卷点击率排行榜。
     *
     * @return {@linkplain cn.dub.entity.manager.survey.Survey}问卷列表
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    public List<Survey> selectsTop();

    /**
     * 根据主键更新题目数。
     *
     * @param id
     *            主键
     * @return 是否更新成功
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    public int updateQueNumById(String id);

    /**
     * 根据主键更新答卷数。
     *
     * @param id
     *            主键
     * @return 是否更新成功
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    public int updateUsehitsById(String id);

    /**
     * 根据主键更新点击率。
     *
     * @param id
     *            主键
     * @return 是否更新成功
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    public int updateHitsById(String id);
}
