/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.dao.manager.survey;

import java.util.List;

import cn.dub.common.annotation.MybatisDao;
import cn.dub.entity.manager.survey.Options;

/**
 * 选项表数据访问接口。
 * 
 * @author daervin (2015年5月13日 下午1:10:34)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
@MybatisDao
public interface IOptionsMybatisDao {

    /**
     * 批量新增选项。
     *
     * @param options
     *            {@linkplain cn.dub.entity.manager.survey.Options} 选项列表
     * @return 受影响行数
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    public int insert(List<Options> options);

    /**
     * 根据ID批量更新被选次数。
     *
     * @param ids
     *            ID列表
     * @return 受影响行数
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    public int updatesUseNumById(List<String> ids);

}
