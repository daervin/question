/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.dao.manager.survey;

import java.util.List;

import cn.dub.common.annotation.MybatisDao;
import cn.dub.entity.manager.survey.Question;

/**
 * 问题表数据访问接口。
 * 
 * @author daervin (2015年5月13日 下午12:28:16)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
@MybatisDao
public interface IQuestionMybatisDao {

    /**
     * 新增问题。
     *
     * @param question
     *            {@linkplain cn.dub.entity.manager.survey.Question} 问题
     * @return 受影响行数
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    public int insert(Question question);

    /**
     * 根据问卷ID查找问题。
     *
     * @param sid
     *            问卷ID
     * @return {@linkplain cn.dub.entity.manager.survey.Question} 问题列表
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    public List<Question> selectsBySid(String sid);
}
