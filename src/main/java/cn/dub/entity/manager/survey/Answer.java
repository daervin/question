/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.entity.manager.survey;

import cn.dub.common.entity.IdEntity;

/**
 * 答案实体类。
 * 
 * @author daervin (2015年5月14日 下午8:01:29)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
public class Answer extends IdEntity {

    private static final long serialVersionUID = -3389983298640599157L;

    private String surveyId; // 问卷ID,外键
    private String userId; // 所属用户的ID,外键
    private String result; // 单个用户的答案（题号(qnum):选项答案(oprelt)=答案1,答案2;文本答案(text)=输入的文本|#|题号:选项答案=答案1,答案2;文本答案=输入的文本）
    private Long postTime; // 答卷提交时间
    private Boolean isDel; // 是否已删除

    public String getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(String surveyId) {
        this.surveyId = surveyId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Long getPostTime() {
        return postTime;
    }

    public void setPostTime(Long postTime) {
        this.postTime = postTime;
    }

    public Boolean getIsDel() {
        return isDel;
    }

    public void setIsDel(Boolean isDel) {
        this.isDel = isDel;
    }

}
