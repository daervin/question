/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.entity.manager.survey;

import java.util.List;

import cn.dub.common.entity.IdEntity;

/**
 * 题目实体类。
 * 
 * @author daervin (2015年5月12日 上午11:08:44)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
public class Question extends IdEntity {

    private static final long serialVersionUID = -2559002340132384503L;

    private String surveyId; // 问卷ID,外键
    private QuestionType type; // 类型（0：问答，1：单选，2：多选，3：单选+自定义输入，4：多选+自定义输入）
    private String head; // 题干
    private Boolean isDel; // 是否已删除
    private Long createTime; // 创建时间

    private List<Options> options; // 当前问题的选项

    /**
     * 题目类型枚举。
     * 
     * @author daervin (2015年5月12日 上午11:19:13)
     *
     * @since 1.0.0
     *
     * @version 1.0.0
     *
     */
    public static enum QuestionType {
        /** 问答 **/
        QUESTION_ANSWER,
        /** 单选 **/
        SINGLE,
        /** 多选 **/
        MULTIPLE,
        /** 单选+自定义输入 **/
        SINGLE_CUSTOM,
        /** 多选+自定义输入 **/
        MULTIPLE_CUSTOM;
    }

    public String getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(String surveyId) {
        this.surveyId = surveyId;
    }

    public QuestionType getType() {
        return type;
    }

    public void setType(QuestionType type) {
        this.type = type;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public Boolean getIsDel() {
        return isDel;
    }

    public void setIsDel(Boolean isDel) {
        this.isDel = isDel;
    }

    public List<Options> getOptions() {
        return options;
    }

    public void setOptions(List<Options> options) {
        this.options = options;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

}
