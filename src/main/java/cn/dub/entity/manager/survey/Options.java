/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.entity.manager.survey;

import cn.dub.common.entity.IdEntity;

/**
 * 选项实体类。
 * 
 * @author daervin (2015年5月12日 上午11:25:10)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
public class Options extends IdEntity {

    private static final long serialVersionUID = 6796456648267492516L;

    private String surveyId; // 问卷ID,外键
    private String questionId; // 题目ID,外键
    private String content; // 选项内容
    private String img; // 选项展示图片
    private Integer orderNum; // 选项序号
    private Integer useNum; // 被选次数
    private Boolean isDel; // 是否已删除

    public String getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(String surveyId) {
        this.surveyId = surveyId;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Integer getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    public Integer getUseNum() {
        return useNum;
    }

    public void setUseNum(Integer useNum) {
        this.useNum = useNum;
    }

    public Boolean getIsDel() {
        return isDel;
    }

    public void setIsDel(Boolean isDel) {
        this.isDel = isDel;
    }

}
