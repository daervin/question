/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.entity.manager.survey;

import cn.dub.common.entity.IdEntity;

/**
 * 问卷类。
 * 
 * @author daervin (2015年5月11日 下午12:26:03)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
public class Survey extends IdEntity {

    private static final long serialVersionUID = 8942248358651110844L;

    private String templetId; // 模板ID,外键
    private String title; // 问卷标题
    private String sdesc; // 问卷描述
    private String author; // 问卷所属用户的用户名
    private String authorId; // 问卷所属用户的ID
    private Long createTime; // 用户创建时间
    private Long expireTime; // 问卷结束时间
    private String password; // 非公开问卷访问密码
    private Boolean isOpen; // 是否公开
    private Integer hits; // 浏览数
    private Integer usehits; // 答卷数
    private Integer queNum; // 题目数
    private Boolean isDel; // 是否已删除

    public String getTempletId() {
        return templetId;
    }

    public void setTempletId(String templetId) {
        this.templetId = templetId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSdesc() {
        return sdesc;
    }

    public void setSdesc(String sdesc) {
        this.sdesc = sdesc;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Long expireTime) {
        this.expireTime = expireTime;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(Boolean isOpen) {
        this.isOpen = isOpen;
    }

    public Integer getHits() {
        return hits;
    }

    public void setHits(Integer hits) {
        this.hits = hits;
    }

    public Integer getUsehits() {
        return usehits;
    }

    public void setUsehits(Integer usehits) {
        this.usehits = usehits;
    }

    public Integer getQueNum() {
        return queNum;
    }

    public void setQueNum(Integer queNum) {
        this.queNum = queNum;
    }

    public Boolean getIsDel() {
        return isDel;
    }

    public void setIsDel(Boolean isDel) {
        this.isDel = isDel;
    }

}
