/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.entity.manager.admin;

import cn.dub.common.entity.IdEntity;

/**
 * 用户实体类。
 *
 * @author daervin(2015年2月11日 下午3:50:50)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
public class User extends IdEntity {

    private static final long serialVersionUID = 4480062779787868224L;
    
    private String email; // 邮箱（用户名）
    private String nickname; // 昵称
    private String password; // 密码
    private Integer age; // 年龄
    private Gender gender; // 性别
    private String address; // 地址
    private String intro; // 介绍
    private Integer money; // 金币
    private Integer level; // 等级
    private String iconPath; // 头像地址
    private Boolean isAdmin; // 是否为超级管理员
    private Long createTime; // 创建时间
    private Boolean isDel; // 是否删除

    public enum Gender {
        /** 保密 **/
        SECRET,
        /** 男 **/
        MALE,
        /** 女 **/
        FEMALE;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public Integer getMoney() {
        return money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getIconPath() {
        return iconPath;
    }

    public void setIconPath(String iconPath) {
        this.iconPath = iconPath;
    }

    public Boolean getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Boolean getIsDel() {
        return isDel;
    }

    public void setIsDel(Boolean isDel) {
        this.isDel = isDel;
    }

}
