/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.common.annotation;

/**
 * 自定义注解，用于标识DAO接口 便于Spring扫描这些接口 注意：该注解仅仅是给Spring扫描DAO。
 *
 * @author daervin(2015年3月10日 下午3:52:49)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
public @interface MybatisDao {
}
