/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.common.shiro;

import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;

import cn.dub.entity.show.User;
import cn.dub.service.show.IUserService;

/**
 * 自定义Realm类。
 *
 * @author daervin(2015年3月12日 上午11:20:01)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */

public class MonitorRealm extends AuthorizingRealm {

    @Inject
    private IUserService userService;

    /*
     * @Autowired UserService userService;
     * 
     * @Autowired RoleService roleService;
     * 
     * @Autowired LoginLogService loginLogService;
     */

    public MonitorRealm() {
        super();
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        /* 这里编写授权代码 */
        Set<String> roleNames = new HashSet<String>();
        Set<String> permissions = new HashSet<String>();
        roleNames.add("admin");
        permissions.add("user.do?myjsp");
        permissions.add("login.do?main");
        permissions.add("login.do?logout");
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo(roleNames);
        info.setStringPermissions(permissions);
        return info;

    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken) throws AuthenticationException {
        /* 这里编写认证代码 */
        UsernamePasswordToken token = (UsernamePasswordToken) authcToken;

        User user = userService.findByEmail(token.getUsername());

        if (null == user) {
            throw new UnknownAccountException("用户名或密码错误");
        }

        ShiroUser shiroUser = new ShiroUser();

        shiroUser.setEmail(user.getEmail());
        shiroUser.setPassword(user.getPassword());
        shiroUser.setAddress(user.getAddress());
        shiroUser.setAge(user.getAge());
        shiroUser.setCreateTime(user.getCreateTime());
        shiroUser.setGender(user.getGender());
        shiroUser.setIconPath(user.getIconPath());
        shiroUser.setId(user.getId());
        shiroUser.setIntro(user.getIntro());
        shiroUser.setIsAdmin(user.getIsAdmin());
        shiroUser.setIsDel(user.getIsDel());
        shiroUser.setLevel(user.getLevel());
        shiroUser.setMoney(user.getMoney());
        shiroUser.setNickname(user.getNickname());

        return new SimpleAuthenticationInfo(shiroUser, shiroUser.getPassword(), getName());

    }

    public void clearCachedAuthorizationInfo(String principal) {
        SimplePrincipalCollection principals = new SimplePrincipalCollection(principal, getName());
        clearCachedAuthorizationInfo(principals);
    }

}
