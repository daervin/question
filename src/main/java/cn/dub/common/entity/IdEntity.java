/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.common.entity;

import java.io.Serializable;

/**
 * 所有实体类的基类。
 * 
 * @author daervin (2015年4月19日 下午9:38:32)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
public abstract class IdEntity implements Serializable {

    private static final long serialVersionUID = 7929246468459764929L;

    protected String id; // 主键

    /**
     * 获取主键。
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    public String getId() {
        return id;
    }

    /**
     * 设置主键。
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    public void setId(String id) {
        this.id = id;
    }

}
