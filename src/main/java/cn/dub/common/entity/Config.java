/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.common.entity;

import java.io.Serializable;

public class Config implements Serializable {

    private Config() {
    }

    private static final long serialVersionUID = 1622845905931301976L;

    public static final int PAGE_NUMBER = 1; // 默认页码
    public static final int PAGE_SIZE = 5; // 默认本页记录数
}
