/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.common.entity;

import java.io.Serializable;

/**
 * 校验错误消息实体类。
 * 
 * @author daervin (2015年5月13日 上午10:40:21)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
public class ValidError implements Serializable {

    private static final long serialVersionUID = -5404105807633059925L;

    private String field;
    private String message;

    public ValidError() {
        super();
    }

    public ValidError(String field, String message) {
        super();
        this.field = field;
        this.message = message;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
