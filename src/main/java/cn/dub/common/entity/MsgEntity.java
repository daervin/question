/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.common.entity;

import java.io.Serializable;

/**
 * 返回给ajax的接送包装数据。
 * 
 * @author daervin (2015年5月11日 下午3:53:11)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
public class MsgEntity implements Serializable {

    private static final long serialVersionUID = 4123510188132741951L;

    private boolean status; // 状态
    private String msg; // 提示信息
    private Object others; // 其他提示

    public MsgEntity() {
        super();
    }

    public MsgEntity(boolean status, String msg) {
        super();
        this.status = status;
        this.msg = msg;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getOthers() {
        return others;
    }

    public void setOthers(Object others) {
        this.others = others;
    }

}
