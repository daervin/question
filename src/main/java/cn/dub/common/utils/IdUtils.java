/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.common.utils;

import org.bson.types.ObjectId;

/**
 * 主键生成工具类。
 * 
 * @author daervin (2015年4月19日 下午10:01:17)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
public final class IdUtils {

    /**
     * 工具类不可实例化。
     */
    private IdUtils() {
    }

    /**
     * 获取主键，使用mongoDB的ObjectId。
     * 
     * @return 主键
     * 
     * @since 1.0.0
     * @version 1.0.0
     */
    public static String getId() {
        return ObjectId.get().toHexString();
    }

}
