/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.common.utils;

import java.text.SimpleDateFormat;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 日期时间工具类。
 * 
 * @author daervin (2015年4月19日 下午9:05:29)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
public final class DateTimeUtils {

    private static final Logger logger = LoggerFactory.getLogger(DateTimeUtils.class);

    /** 时间格式（yyyy-MM-dd HH:mm） */
    public static final String LONG_TIME_STR = "yyyy-MM-dd HH:mm";

    /** SimpleDateFormat（yyyy-MM-dd HH:mm） */
    public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat(LONG_TIME_STR);

    // 让工具类彻底不可以实例化
    private DateTimeUtils() {
        throw new Error("工具类不可以实例化！");
    }

    /**
     * 将字符串形式的时间（yyyy-MM-dd HH:mm）转换为毫秒
     * 
     * @param strDateTime
     *            字符串形式的时间（yyyy-MM-dd HH:mm）
     * @return 毫秒
     * 
     * @since 1.0.0
     * @version 1.0.0
     */
    public static long strDateTimeToMillis(String strDateTime) {
        long millis = -1;

        if (StringUtils.isNotBlank(strDateTime)) {
            try {
                millis = SIMPLE_DATE_FORMAT.parse(strDateTime).getTime();
            }
            catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }

        return millis;
    }

}
