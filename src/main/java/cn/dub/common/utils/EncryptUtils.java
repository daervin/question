/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.common.utils;

import org.apache.shiro.crypto.hash.Md5Hash;

/**
 * MD5工具类。
 * 
 * @author daervin (2015年4月19日 下午9:02:00)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
public final class EncryptUtils {

    /**
     * 工具类不可实例化。
     */
    private EncryptUtils() {
    }

    public static final String encryptMD5(String source) {
        if (source == null) {
            source = "";
        }
        Md5Hash md5 = new Md5Hash(source);
        return md5.toString();
    }

    /**
     * 测试生产密文。
     * 
     * @since 1.0.0
     * @version 1.0.0
     */
    public static void main(String[] args) {
        System.out.println(EncryptUtils.encryptMD5("111111"));
    }
}
