/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.common.utils;

import org.apache.shiro.SecurityUtils;

import cn.dub.common.shiro.ShiroUser;

/**
 * 获取当前用户信息工具类。
 * 
 * @author daervin (2015年4月19日 下午9:12:48)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
public final class CurrentUserUtils {

    /**
     * 工具类不可实例化。
     */
    private CurrentUserUtils() {
    }

    /**
     * 获取当前用户。
     *
     * @return {@linkplain cn.dub.common.shiro.ShiroUser} 当前用户实体
     *
     */
    public static ShiroUser getCurrentUser() {
        ShiroUser shiroUser = (ShiroUser) SecurityUtils.getSubject().getPrincipal();
        return shiroUser;
    }
}
