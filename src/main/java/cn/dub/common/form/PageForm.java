/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.common.form;

/**
 * 分页数据公共FORM。
 * 
 * @author daervin (2015年5月14日 上午9:41:32)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
public class PageForm {

    private String pageNumber; // 页码
    private String pageSize; // 页记录

    public String getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(String pageNumber) {
        this.pageNumber = pageNumber;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

}
