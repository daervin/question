/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.service.manager.survey;

import java.util.List;

import cn.dub.entity.manager.survey.Survey;
import cn.dub.form.manager.survey.AddSurveyForm;
import cn.dub.form.manager.survey.FindSurveyListForm;

import com.github.pagehelper.PageInfo;

/**
 * 问卷中心业务逻辑接口。
 * 
 * @author daervin (2015年5月11日 下午2:57:53)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
public interface ISurveyService {

    /**
     * 新增空白问卷。
     *
     * @param form
     *            {@linkplain cn.dub.form.manager.survey.AddSurveyForm}表单数据
     * @return 新问卷ID
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    public String addSurvey(AddSurveyForm form);

    /**
     * 根据主键查找单个问卷。
     * 
     * @param id
     *            主键
     * @return {@linkplain cn.dub.entity.manager.survey.Survey}问卷实体类
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    public Survey findById(String id);

    /**
     * 根据主键查找单个问卷详情。
     * 
     * @param id
     *            主键
     * @return {@linkplain cn.dub.entity.manager.survey.Survey}问卷实体类
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    public Survey detailsById(String id);

    /**
     * 根据参数查找问卷列表（参数均为空则查全部）。
     *
     * @param form
     *            表单数据
     * @return {@linkplain cn.dub.entity.manager.survey.Survey}问卷列表
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    public PageInfo<Survey> findsAllByParameter(FindSurveyListForm form);

    /**
     * 问卷点击率排行榜。
     *
     * @return {@linkplain cn.dub.entity.manager.survey.Survey}问卷列表
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    public List<Survey> findsTop();

    /**
     * 所以问卷。
     *
     * @return {@linkplain cn.dub.entity.manager.survey.Survey}问卷列表
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    public List<Survey> findsAll();

}
