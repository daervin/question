/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.service.manager.survey.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import cn.dub.common.utils.Clock;
import cn.dub.common.utils.IdUtils;
import cn.dub.dao.manager.survey.IOptionsMybatisDao;
import cn.dub.dao.manager.survey.IQuestionMybatisDao;
import cn.dub.dao.manager.survey.ISurveyMybatisDao;
import cn.dub.entity.manager.survey.Options;
import cn.dub.entity.manager.survey.Question;
import cn.dub.entity.manager.survey.Question.QuestionType;
import cn.dub.form.manager.survey.AddQuestionAndOptionsForm;
import cn.dub.service.manager.survey.IQuestionService;

/**
 * 问卷中心问卷题目业务逻辑实现。
 * 
 * @author daervin (2015年5月13日 下午12:27:20)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
@Service
public class QuestionServiceImpl implements IQuestionService {

    @Inject
    private IQuestionMybatisDao questionDao;

    @Inject
    private IOptionsMybatisDao optionDao;

    @Inject
    private ISurveyMybatisDao surveyDao;

    private Clock clock = Clock.DEFAULT;

    public void setClock(Clock clock) {
        this.clock = clock;
    }

    @Override
    public boolean add(AddQuestionAndOptionsForm form) {
        String surveyId = form.getSurveyId(); // 问卷ID,外键
        Integer type = Integer.parseInt(form.getType()); // 类型（0：问答，1：单选，2：多选，3：单选+自定义输入，4：多选+自定义输入）
        String head = form.getHead(); // 题干

        String contentA = form.getContentA(); // 选项内容A
        String imgA = form.getImgA(); // 选项展示图片A
        String orderNumA = form.getOrderNumA(); // 选项序号A

        String contentB = form.getContentB(); // 选项内容B
        String imgB = form.getImgB(); // 选项展示图片B
        String orderNumB = form.getOrderNumB(); // 选项序号B

        String contentC = form.getContentC(); // 选项内容C
        String imgC = form.getImgC(); // 选项展示图片C
        String orderNumC = form.getOrderNumC(); // 选项序号C

        String contentD = form.getContentD(); // 选项内容D
        String imgD = form.getImgD(); // 选项展示图片D
        String orderNumD = form.getOrderNumD(); // 选项序号D

        String contentE = form.getContentE(); // 选项内容E
        String imgE = form.getImgE(); // 选项展示图片E
        String orderNumE = form.getOrderNumE(); // 选项序号E

        String contentF = form.getContentF(); // 选项内容F
        String imgF = form.getImgF(); // 选项展示图片F
        String orderNumF = form.getOrderNumF(); // 选项序号F

        String questionId = IdUtils.getId();
        Question question = new Question();
        question.setId(questionId);
        question.setHead(head);
        question.setType(QuestionType.values()[type]);
        question.setSurveyId(surveyId);
        question.setCreateTime(this.clock.getCurrentTimeInMillis());
        boolean success = this.questionDao.insert(question) > 0; // 新增问题

        List<Options> options = new ArrayList<Options>();
        this.createoptions(contentA, orderNumA, imgA, questionId, surveyId, options);
        this.createoptions(contentB, orderNumB, imgB, questionId, surveyId, options);
        this.createoptions(contentC, orderNumC, imgC, questionId, surveyId, options);
        this.createoptions(contentD, orderNumD, imgD, questionId, surveyId, options);
        this.createoptions(contentE, orderNumE, imgE, questionId, surveyId, options);
        this.createoptions(contentF, orderNumF, imgF, questionId, surveyId, options);

        if (options.size() > 0) {
            success = this.optionDao.insert(options) > 0; // 新增选项
        }

        success = this.surveyDao.updateQueNumById(surveyId) > 0; // 更新该问卷的题目总数

        return success;
    }

    @Override
    public List<Question> findsQuestion(String sid) {
        return this.questionDao.selectsBySid(sid);
    }

    /**
     * 创建选项列表。
     *
     * @return 选项列表
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    private void createoptions(String contentA, String orderNumA, String imgA, String questionId, String surveyId, List<Options> options) {
        if (StringUtils.isNotBlank(contentA) && StringUtils.isNotBlank(orderNumA)) {
            Options option = new Options();
            option.setId(IdUtils.getId());
            option.setImg(imgA);
            option.setContent(contentA);
            option.setOrderNum(Integer.parseInt(orderNumA));
            option.setQuestionId(questionId);
            option.setSurveyId(surveyId);

            options.add(option);
        }

    }

}
