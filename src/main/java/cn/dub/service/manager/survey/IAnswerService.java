/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.service.manager.survey;

import cn.dub.form.manager.survey.AddAnswerForm;

/**
 * 答卷业务逻接口。
 * 
 * @author daervin (2015年5月15日 下午1:04:28)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
public interface IAnswerService {

    /**
     * 新增答卷。
     *
     * @param answer
     *            表单数据
     * @return 受影响行数
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    public boolean insert(AddAnswerForm form);
}
