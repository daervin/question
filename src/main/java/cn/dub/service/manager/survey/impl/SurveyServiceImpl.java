/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.service.manager.survey.impl;

import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import cn.dub.common.entity.Config;
import cn.dub.common.shiro.ShiroUser;
import cn.dub.common.utils.Clock;
import cn.dub.common.utils.CurrentUserUtils;
import cn.dub.common.utils.DateTimeUtils;
import cn.dub.common.utils.EncryptUtils;
import cn.dub.common.utils.IdUtils;
import cn.dub.dao.manager.survey.ISurveyMybatisDao;
import cn.dub.entity.manager.survey.Survey;
import cn.dub.form.manager.survey.AddSurveyForm;
import cn.dub.form.manager.survey.FindSurveyListForm;
import cn.dub.service.manager.survey.ISurveyService;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

/**
 * 问卷中心业务逻辑实现。
 * 
 * @author daervin (2015年5月11日 下午3:01:26)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
@Service
public class SurveyServiceImpl implements ISurveyService {

    @Inject
    private ISurveyMybatisDao surveyDao;

    private Clock clock = Clock.DEFAULT;

    public void setClock(Clock clock) {
        this.clock = clock;
    }

    /**
     * 新增问卷。
     */
    @Override
    public String addSurvey(AddSurveyForm form) {
        String title = form.getTitle();// 问卷标题
        String sdesc = form.getSdesc();// 问卷描述
        Long expireTime = DateTimeUtils.strDateTimeToMillis(form.getExpireTime());// 问卷结束时间
        Boolean isOpen = Boolean.parseBoolean(form.getIsOpen());// 是否公开
        String password = form.getPassword();// 非公开问卷访问密码

        // 非公开则更新访问密码
        if (!isOpen) {
            password = EncryptUtils.encryptMD5(password);
        }

        Long createTime = this.clock.getCurrentTimeInMillis(); // 当前时间
        ShiroUser user = CurrentUserUtils.getCurrentUser(); // 获取当前用户信息
        // 将信息set给Survey
        Survey survey = new Survey();
        survey.setId(IdUtils.getId());
        survey.setTitle(title);
        survey.setSdesc(sdesc);
        survey.setAuthor(user.getNickname());
        survey.setAuthorId(user.getId());
        survey.setCreateTime(createTime);
        survey.setExpireTime(expireTime);
        survey.setPassword(password);
        survey.setIsOpen(isOpen);

        String id = null;
        if (this.surveyDao.insert(survey) > 0) {
            id = survey.getId();
        }
        return id;
    }

    /**
     * 根据主键查找单个问卷。
     */
    @Override
    public Survey findById(String id) {
        return this.surveyDao.selectById(id);
    }

    /**
     * 单个问卷详情。
     */
    @Override
    public Survey detailsById(String id) {
        Survey survey = this.surveyDao.selectById(id);
        if (survey != null) {
            this.surveyDao.updateHitsById(id);
        }
        return survey;
    }

    /***
     * 根据参数查找问卷列表（参数均为空则查全部）。
     */
    @Override
    public PageInfo<Survey> findsAllByParameter(FindSurveyListForm form) {
        int pageNumber = StringUtils.isNumeric(form.getPageNumber()) ? Integer.parseInt(form.getPageNumber()) : Config.PAGE_NUMBER; // 页码
        int pageSize = StringUtils.isNumeric(form.getPageSize()) ? Integer.parseInt(form.getPageSize()) : Config.PAGE_SIZE; // 本页显示记录数
        String title = form.getTitle();
        String isOpeStr = form.getIsOpen();

        Boolean isOpen = null;
        if (StringUtils.isNotBlank(isOpeStr)) {
            isOpen = Boolean.parseBoolean(isOpeStr);
        }

        PageHelper.startPage(pageNumber, pageSize); // 调用分页方法 紧跟其后的一个select被分页
        List<Survey> list = this.surveyDao.selectsAllByParameter(title, isOpen, null);

        return new PageInfo<Survey>(list);
    }

    /**
     * 问卷点击率排行榜。
     */
    @Override
    public List<Survey> findsTop() {
        return this.surveyDao.selectsTop();
    }

    @Override
    public List<Survey> findsAll() {
        return this.surveyDao.selectsAllByParameter(null, null, null);
    }

}
