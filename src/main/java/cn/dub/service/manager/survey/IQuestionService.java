/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.service.manager.survey;

import java.util.List;

import cn.dub.entity.manager.survey.Question;
import cn.dub.form.manager.survey.AddQuestionAndOptionsForm;

/**
 * 问卷中心问卷题目业务逻辑接口。
 * 
 * @author daervin (2015年5月13日 下午12:23:55)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
public interface IQuestionService {

    /**
     * 新增问题。
     *
     * @param form
     *            表单数据
     * @return 是否新增成功
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    public boolean add(AddQuestionAndOptionsForm form);

    /**
     * 根据问卷ID查找问题。
     *
     * @param sid
     *            问卷ID
     * @return {@linkplain cn.dub.entity.manager.survey.Question} 问题列表
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    public List<Question> findsQuestion(String sid);
}
