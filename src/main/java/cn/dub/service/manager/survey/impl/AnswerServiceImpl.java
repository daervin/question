/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.service.manager.survey.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.dub.common.shiro.ShiroUser;
import cn.dub.common.utils.Clock;
import cn.dub.common.utils.CurrentUserUtils;
import cn.dub.common.utils.IdUtils;
import cn.dub.dao.manager.survey.IAnswerMybatisDao;
import cn.dub.dao.manager.survey.IOptionsMybatisDao;
import cn.dub.dao.manager.survey.ISurveyMybatisDao;
import cn.dub.entity.manager.survey.Answer;
import cn.dub.form.manager.survey.AddAnswerForm;
import cn.dub.service.manager.survey.IAnswerService;

/**
 * 答卷业务逻实现。
 * 
 * @author daervin (2015年5月15日 下午1:04:18)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
@Service
public class AnswerServiceImpl implements IAnswerService {

    @Inject
    private IAnswerMybatisDao answerDao;

    @Inject
    private ISurveyMybatisDao surveyDao;

    @Inject
    private IOptionsMybatisDao optionsDao;

    private Clock clock = Clock.DEFAULT;

    public void setClock(Clock clock) {
        this.clock = clock;
    }

    /**
     * 新增答卷。
     */
    @Override
    @Transactional
    public boolean insert(AddAnswerForm form) {
        String surveyId = form.getSurveyId(); // 问卷ID
        String result = form.getResult(); // 答案信息

        ShiroUser user = CurrentUserUtils.getCurrentUser(); // 获取当前登录用户信息

        Answer answer = new Answer();
        answer.setId(IdUtils.getId());
        answer.setPostTime(this.clock.getCurrentTimeInMillis());
        answer.setSurveyId(surveyId);
        answer.setResult(result);
        answer.setUserId(user.getId());

        // 处理单个用户只能对单个问卷填写一次
        boolean success = this.answerDao.selectCount(answer) > 0;
        if (success) {
            return !success;
        }

        success = this.answerDao.insert(answer) > 0; // 新增答卷

        if (success) {
            success = this.surveyDao.updateUsehitsById(surveyId) > 0;// 更新该问卷的答卷总数
            success = this.updateSummary(answer); // 更新选项被选次数
        }

        return success;
    }

    /**
     * 更新选项被选次数。
     *
     * @param answer
     *            答案实体。
     * @return 是否更新成功。
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    @Transactional
    public boolean updateSummary(Answer answer) {
        List<String> idList = new ArrayList<String>(); // 用于存放选项ID

        String[] singleResult = StringUtils.split(answer.getResult(), "|#|"); // 单条答案

        for (String s : singleResult) { // 遍历单条答案的单条选项
            // String queId = StringUtils.substringBefore(s, ":"); // 当前问题ID
            String oprelt = StringUtils.substringBetween(s, "oprelt=", ";"); // 当前问题的答案
            String[] opreltArr = StringUtils.split(oprelt, ","); // 当前问题的答案数组

            for (String oprelId : opreltArr) { // 遍历得到所选选项ID
                idList.add(oprelId);
            }
        }

        boolean success = false;
        if (idList.size() > 0) {
            success = this.optionsDao.updatesUseNumById(idList) > 0; // 更新选项被选次数
        }

        return success;
    }
}
