/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.service.manager.admin;

import java.util.List;

import cn.dub.entity.show.User;

/**
 * 超级管理员用户管理业务逻辑接口。
 * 
 * @author daervin (2015年5月18日 下午3:31:01)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
public interface IManagerUserService {

    /**
     * 查询所有用户。
     *
     * @return 用户列表
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    public List<User> findsUser();

    /**
     * 根据ID删除用户。
     *
     * @param id
     *            用户ID
     * @return 是否更新成功
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    public boolean deleteById(String id);
}
