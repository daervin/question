/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.service.manager.admin.impl;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import cn.dub.dao.show.IUserMybatisDao;
import cn.dub.entity.show.User;
import cn.dub.service.manager.admin.IManagerUserService;

/**
 * 超级管理 用户管理业务逻辑实现。
 * 
 * @author daervin (2015年5月18日 下午3:35:21)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */
@Service
public class ManagerUserServiceImpl implements IManagerUserService {

    @Inject
    private IUserMybatisDao userDao;

    @Override
    public List<User> findsUser() {
        List<User> list = this.userDao.selectsAll();
        return list;
    }

    @Override
    public boolean deleteById(String id) {
        return this.userDao.deleteById(id) > 0;
    }

}
