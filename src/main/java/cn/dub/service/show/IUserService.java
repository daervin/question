/**
 * <p>Project: questionnaire</p>
 * <p>Copyright: Copyright (c) 2015 7dub.cn</p>
 * <p>Company: 柒色都吧7dub.cn</p>
 */

package cn.dub.service.show;

import cn.dub.entity.show.User;
import cn.dub.form.show.session.EditForm;
import cn.dub.form.show.session.RegisterForm;

/**
 * 用户业务逻辑接口。
 *
 * @author daervin(2015年3月12日 下午4:44:23)
 *
 * @since 1.0.0
 *
 * @version 1.0.0
 *
 */

public interface IUserService {

    /**
     * 新增用户。
     *
     * @param form
     *            {@linkplain cn.dub.form.show.session.RegisterForm} 表单数据
     * @return 是否新增成功
     *
     */
    public boolean insert(RegisterForm form);

    /**
     * 根据邮箱查找用户实体。
     * 
     * @param email
     *            邮箱
     * @return {@linkplain cn.dub.entity.show.User}对象
     */
    public User findByEmail(String email);

    /**
     * 更新资料。
     *
     * @param form
     *            表单数据
     * @return 是否更新成功
     *
     * @since 1.0.0
     * @version 1.0.0
     */
    public boolean editCurrentUser(EditForm form);

}
